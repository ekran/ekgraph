from math import *

grid = []
size = 0
qsize = 0

def gengrid(gridsize):
    global grid
    global size
    global qsize
    qsize = gridsize
    size = gridsize*2
    grid = []
    for i in range(0,size+2):
        grid.append([])
    for i in range(0, size+2):
        for i in range(0,size+2):
            grid[i].append(" ")
    
def linear(coef,icept):
    global qsize
    global grid
    global size
    for i in range(1,size+2):
        try:
            x = i-(qsize+1)
            y = round((coef*x)+icept)
            if (y+qsize) > size or y+qsize < 0:
                pass
            else:
                grid[i][y+qsize+1] = "#"
        except:
            pass
    quads()
    
def quadratic(a,b,c):
    global qsize
    global grid
    global size
    for i in range(1,size):
        try:
            x = i-(qsize+1)
            y = round((a*(x**2))+(b*x)+c)
            if (y+qsize) > size or y+qsize < 0:
                pass
            else:
                grid[i][y+qsize+1] = "#"
        except:
            pass
    quads()

def quads():
    global qsize
    global grid
    global size
    print("┌"+"─"*(qsize)+"┬"+"─"*(qsize)+"┐")
    for i in range(size+1,qsize+1,-1):
        print("│",end="")
        for o in range(1,qsize+1):
            print(grid[o][i],end="")        #top left quadrant
        print("│",end="")   
        for o in range(qsize+2,size+1):
            print(grid[o][i],end="")        #top right quadrant
        print(grid[size+1][i],end="")
        print("│")
    print("├"+"─"*(qsize)+"┼"+"─"*(qsize)+"┼"+str(qsize))
    for i in range(qsize,0,-1):
        print("│",end="")
        for o in range(1,qsize+1):
            print(grid[o][i],end="")          #bottom left quadrant
        print("│",end="")   
        for o in range(qsize+2,size+1):
            print(grid[o][i],end="")            #bottom right quadrant
        print(grid[size+1][i],end="")
        print("│")
    print("└"+"─"*(qsize)+"┴"+"─"*(qsize)+"┘")

def custom(func):
    global qsize
    global grid
    global size
    for i in range(1,size):
        try:
            x = i-(qsize+1)
            exec("y = round("+func+")", locals(), globals())
            if (y+qsize) > size or y+qsize < 0:
                pass
            else:
                grid[i][y+qsize+1] = "#"
        except:
            pass
    quads()

def point(x,y,symbol):
    global qsize
    global grid
    global size
    grid[x+qsize+1][y+qsize+1] = symbol
    quads()

def main():
    print("\nWelcome to ekGraph v1.0.0\n")
    print("Enter a number to draw a line or point\n")
    print("1. Point\n2. Linear\n3. Quadratic\n4. Custom\n5. Reset\n")
    choice = input()
    if choice == "1":
        try:
            px = round(float(input("\nEnter X value: ")))
            py = round(float(input("Enter Y value: ")))
            ps = input("Enter symbol for point: ")
            if len(ps) != 1:
                print("Enter a single character")
                main()
            else:
                point(px,py,ps)
                main()
        except:
            print("Enter valid number")
            main()
    elif choice == "2":
        try:
            px = float(input("\nEnter coefficient: "))
            py = float(input("Enter y axis intercept: "))
        except:
            print("Enter valid number")
            main()
        linear(px,py)
        main()
    elif choice == "3":
        try:
            pa = float(input("\nEnter A: "))
            pb = float(input("Enter B: "))
            pc = float(input("Enter C: "))
        except:
            print("Enter valid number")
            main()
        quadratic(pa,pb,pc)
        main()
    elif choice == "4":
        pf = input("Enter function: ")
        pt = 0
        for x in range(1,2):
            try:
                exec("pt = round("+pf+")", locals(), globals())
                custom(pf)
                main()
            except:
                print("Enter valid function")
                main()
        
    elif choice == "5":
        ns = input("\nEnter new grid size: ")
        try:
            int(ns); gengrid(int(ns))
            main()
        except:
            print("\nEnter valid integer")
            main()
    else:
        print("Choice unavailable")
        main()

gengrid(30)
main()
