from math import *

grid = []
size = 0
qsize = 0
zoom = 1
eqs = []
pts = []

def gengrid(gridsize,gridzoom):
    global grid
    global size
    global qsize
    global zoom
    global eqs
    global pts
    zoom = gridzoom
    qsize = gridsize
    size = gridsize*2
    grid = []
    for i in range(0,size+2):
        grid.append([])
    for i in range(0, size+2):
        for i in range(0,size+2):
            grid[i].append(" ")
    for i in range(0,size+2):
        grid[i][qsize+1] = "─"
        grid[qsize+1][i] = "│"
    grid[qsize+1][qsize+1] = "┼"
    
def linear(coef,icept):
    global qsize
    global grid
    global size
    global zoom
    global eqs
    global pts
    for i in range(1,size+2):
        try:
            x = (1/zoom)*(i-(qsize+1))
            y = zoom*round((coef*x)+icept,round(log10(zoom)))
            if (y+qsize) > size or y+qsize < 0:
                pass
            else:
                grid[i][round(y+qsize+1)] = "#"
        except:
            pass
    eqs.append("round(("+str(coef)+"*x)+"+str(icept)+",round(log10(zoom)))")
    quads()

def quadratic(a,b,c):
    global qsize
    global grid
    global size
    global zoom
    global eqs
    global pts
    for i in range(1,size+2):
        try:
            x = (1/zoom)*(i-(qsize+1))
            y = zoom*round((a*(x**2))+(b*x)+c,round(log10(zoom)))
            if (y+qsize) > size or y+qsize < 0:
                pass
            else:
                grid[i][round(y+qsize+1)] = "#"
        except:
            pass
    eqs.append("round(("+str(a)+"*(x**2))+("+str(b)+"*x)+"+str(c)+",round(log10(zoom)))")
    quads()

def quads():
    global qsize
    global grid
    global size
    global zoom
    global eqs
    global pts
    lb = "│"
    rb = "│"
    val = ""
    print("┌"+"─"*(qsize)+"┬"+"─"*(qsize)+"┐")
    for i in range(size+1,0,-1):
        if i == qsize+1:
            lb = "├"
            rb = "┼"
            val = str(qsize/zoom)
        else:
            lb = "│"
            rb = "│"
            val = ""
        print(lb,end="")
        for o in range(1,size+2):
            print(grid[o][i],end="")
        print(rb+val)
    print("└"+"─"*(qsize)+"┴"+"─"*(qsize)+"┘")

def custom(func):
    global qsize
    global grid
    global size
    global zoom
    global eqs
    global pts
    for i in range(1,size+2):
        try:
            x = (1/zoom)*(i-(qsize+1))
            exec("y = zoom*round("+func+",round(log10(zoom)))", locals(), globals())
            if (y+qsize) > size or y+qsize < 0:
                pass
            else:
                grid[i][round(y+qsize+1)] = "#"
        except:
            pass
    eqs.append("round("+func+",round(log10(zoom)))")
    quads()

def point(x,y,symbol):
    global qsize
    global grid
    global size
    global zoom
    global eqs
    global pts
    grid[round(zoom*x)+(qsize+1)][round(zoom*y)+(qsize+1)] = symbol
    pts.append("grid[round(zoom*"+str(x)+")+(qsize+1)][round(zoom*"+str(y)+")+(qsize+1)] = \""+symbol+"\"")
    quads()

def main():
    global qsize
    global grid
    global size
    global zoom
    global eqs
    global pts
    print("\nWelcome to ekGraph v1.1.1\n")
    print("Enter a number to draw a line or point\n")
    print("1. Point\n2. Linear\n3. Quadratic\n4. Custom\n5. Zoom in/out\n6. Reset\n")
    choice = input()
    if choice == "1":
        try:
            px = float(input("\nEnter X value: "))
            py = float(input("Enter Y value: "))
            ps = input("Enter symbol for point: ")
            if len(ps) != 1:
                print("Enter a single character")
                main()
            else:
                point(px,py,ps)
                main()
        except:
            print("Enter valid number")
            main()
    elif choice == "2":
        try:
            px = float(input("\nEnter coefficient: "))
            py = float(input("Enter y axis intercept: "))
        except:
            print("Enter valid number")
            main()
        linear(px,py)
        main()
    elif choice == "3":
        try:
            pa = float(input("\nEnter A: "))
            pb = float(input("Enter B: "))
            pc = float(input("Enter C: "))
        except:
            print("Enter valid number")
            main()
        quadratic(pa,pb,pc)
        main()
    elif choice == "4":
        pf = input("Enter function: ")
        pt = 0
        for x in range(1,2):
            try:
                exec("pt = round("+pf+")", locals(), globals())
                custom(pf)
                main()
            except:
                print("Enter valid function")
                main()
    elif choice == "5":
        nz = input("Enter positive integer to zoom in, or negative to zoom out: ")
        try:
            nz = int(nz)
            zoominout(nz)
            pointzoom()
        except:
            print("\nEnter valid integer")
            main()
    elif choice == "6":
        eqs = []
        pts = []
        ns = input("\nEnter new grid size (integer): ")
        nz = input("\nEnter new zoom level (power of 10): ")
        try:
            int(ns)
            float(nz)
            int(log10(float(nz)))
            gengrid(int(ns),float(nz))
            main()
        except:
            print("\nEnter valid number")
            main()
    else:
        print("Choice unavailable")
        main()

def zoominout(pm):
    global qsize
    global grid
    global size
    global zoom
    global eqs
    global pts
    gengrid(qsize,zoom)
    zoom = zoom*(10**pm)
    for i in eqs:
        for o in range(1,size+2):
            try:
                x = (1/zoom)*(o-(qsize+1))
                exec("y = zoom*"+i, locals(), globals())
                if (y+qsize) > size or y+qsize < 0:
                    pass
                else:
                    grid[o][round(y+qsize+1)] = "#"
            except:
                pass
    
def pointzoom():
    global qsize
    global grid
    global size
    global zoom
    global eqs
    global pts
    for i in pts:
        exec(i, locals(), globals())
    quads()
    main()
    
gengrid(30,1)
main()
