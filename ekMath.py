pi = 3.1415926535897932

def pimax(theta):
    if theta > pi:
        while theta > pi:
            theta -= 2*pi
        return theta
    elif theta < -pi:
        while theta < -pi:
            theta += 2*pi
        return theta
    else:
        return theta

def sine(theta):
    total = 0
    for i in range(0,9):
        total += ((-1)**i)*(pimax(theta)**((2*i)+1))/(fact((2*i)+1))
    return round(total,7)

sin = sine

def fact(arg):
    total = 1
    for i in range(1,arg+1):
        total *= i
    return total

def cosine(theta):
    ans = sqrt((1-sine(theta)**2))
    if pimax(theta) > pi/2 or pimax(theta) < -pi/2:
        ans = -ans
    return round(ans,7)

cos = cosine

def tangent(theta):
    try:
        ans = sine(theta)/cosine(theta)
        return round(ans,7)
    except:
        ans = "Undefined"
        return ans

tan = tangent

def secant(theta):
    try:
        ans = 1/cosine(theta)
        return round(ans,7)
    except:
        ans = "Undefined"
        return ans

sec = secant

def cosecant(theta):
    try:
        ans = 1/sine(theta)
        return round(ans,7)
    except:
        ans = "Undefined"
        return ans

csc = cosecant

def cotangent(theta):
    try:
        ans = 1/tangent(theta)
        return round(ans,7)
    except:
        ans = "Undefined"
        return ans

cot = cotangent

def doublefact(arg):
    total = 1
    for i in range(arg,0,-2):
        total *= i
    return total

def squareroot(arg):
    return arg**(1/2)

sqrt = squareroot

def cuberoot(arg):
    return arg**(1/3)

cbrt = cuberoot

def floor(arg):
    if isinstance(arg,int) == True:
        return arg
    else:
        return arg//1

def ceiling(arg):
    if isinstance(arg,int) == True:
        return arg
    else:
        return (arg//1)+1
