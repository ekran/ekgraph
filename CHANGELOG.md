- v1.0.0 - First working version of ekGraph, may contain some bugs if invalid values are entered.
- v1.0.1 - X and Y values of 0 are now rendered instead of the border lines.
- v1.1.0 - You can now set level of zoom on grid generation. Cleaned up some commented out code.
- v1.1.1 - You can now adjust zoom after drawing lines and points
- v1.2.0 - Using ekMath you can now do more trigonometric functions such as secant, cosecant, and cotangent.
           Other new functions include: cuberoot(x), doublefact(x), and floor/ceiling functions
- v1.2.1 - Cleaned up global stacking